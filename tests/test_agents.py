from unittest import TestCase
from network.network import ServerQueue, LGNetworkServer, LGNetworkClient, ClientQueue, NetworkThread, MessageListener


class MyMessageListener(MessageListener):
    def __init__(self):
        super().__init__()


class MyServerQueue(ServerQueue):
    def __init__(self):
        super().__init__()


class MyNetworkServer(LGNetworkServer):
    def __init__(self, server_queue,
                 address: tuple,
                 listener: MessageListener):
        super().__init__(server_queue, address, listener)


class AgentTest(TestCase):
    def setUp(self):
        self._listener = MyMessageListener()
        self._server_queue = MyServerQueue()
        self._network_server = MyNetworkServer()

    def tearDown(self):
        pass

