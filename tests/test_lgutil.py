from unittest import TestCase
from util.LGUtil import ClientQueue, ServerQueue, Context, test_method, implements
from clients.baseclient import BaseClient
import util.LGUtil
class MyClientQueue(ClientQueue):

    def __init__(self):
        super().__init__()


class MyServerQueue(ServerQueue):

    def __init__(self):
        super().__init__()

    def get_element_number(self):
        return len(self._queue)

    def get_messages_of_client(self, client_name: str):
        return self._queue[client_name]


@implements(BaseClient)
class MyWrongTestClient():
    def __init__(self):
        super().__init__()

    @test_method
    def test_method(self):
        pass


@implements(BaseClient)
class MyTestClient():
    def __init__(self):
        super().__init__()

    @test_method
    def test_method(self):
        pass

    @test_method
    def test_method_with_param(self, param1):
        pass

    def initialize(self, parameters: dict, context: Context):
        print('Initialized with parameters:', parameters)

    def uninitialize(self):
        pass

    def statistics(self) -> dict:
        return None


class UtilTest(TestCase):
    def setUp(self):
        self._client_queue = MyClientQueue()
        self._server_queue = MyServerQueue()
        self._context = Context(self._client_queue)

    def tearDown(self):
        pass
    #
    # def test_load_methods_failure(self):
    #     clazz = util.LGUtil.my_import('tests.test_lgutil.MyWrongTestClient')
    #     try:
    #         if clazz is isinstance(MyWrongTestClient):
    #             self.assertTrue(True)
    #         else:
    #             self.fail('Wrong instance')
    #     except NotImplementedError as e:
    #             self.assertTrue(True)

    def test_load_methods(self):
        clazz = util.LGUtil.my_import('tests.test_lgutil.MyTestClient')
        try:
            if clazz is isinstance(MyTestClient):
                self.assertTrue(True)
            else:
                self.fail('Wrong instance')
            methods = util.LGUtil.get_clients_methods(['tests.test_lgutil.MyTestClient'])
            print(methods)
            if len(methods) == 0:
                self.fail('No test method was found!')

        except NotImplementedError as e:
                self.fail('Wrong instance')

    def test_client_queue_add_NONE_message(self):
        self._client_queue.add_message(None)
        self.assertTrue(0 == self._client_queue.messages_number())

    def test_client_queue_add_message(self):
        self._client_queue.add_message('Test msg')
        self.assertTrue(1 == self._client_queue.messages_number())

    def test_client_queue_clear_messages(self):
        self._client_queue.add_message('Test msg')
        self._client_queue.clear_messages()
        self.assertTrue(0 == self._client_queue.messages_number())

    def test_client_queue_pop_message(self):
        self._client_queue.add_message('Test msg')
        self._client_queue.add_message('Test msg2')
        msg = self._client_queue.get_next_message()
        self.assertTrue(msg == 'Test msg')
        self.assertTrue(1 == self._client_queue.messages_number())
        msg = self._client_queue.get_next_message()
        self.assertTrue(msg == 'Test msg2')
        self.assertTrue(0 == self._client_queue.messages_number())

    def test_server_queue_add_None_message(self):
        self._server_queue.add_message(None, None)
        self.assertTrue(0 == self._server_queue.get_element_number())

    def test_server_queue_add_message(self):
        self._server_queue.add_message('a', 'b')
        self.assertTrue(1 == self._server_queue.get_element_number())

    def test_server_queue_add_2_messages(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        msgs = self._server_queue.get_messages_of_client('a')
        self.assertTrue(2 == len(msgs))

    def test_server_queue_add_2_messages_1_pop(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        _msg = self._server_queue.get_next_message('a')
        self.assertTrue('b' == _msg)
        msgs = self._server_queue.get_messages_of_client('a')
        self.assertTrue(1 == len(msgs))

    def test_server_queue_clear(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        self._server_queue.clear_messages('a')
        msgs = self._server_queue.get_messages_of_client('a')
        self.assertTrue(0 == len(msgs))

    def test_server_get_unknown_client(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        self._server_queue.clear_messages('c')
        msgs = self._server_queue.get_messages_of_client('c')
        self.assertTrue(0 == len(msgs))

    def test_context(self):
        self._context.send_message('message1')
        self.assertTrue(1 == self._client_queue.messages_number())
