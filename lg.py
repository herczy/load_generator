import sys
from argparse import ArgumentParser
import logging
import socket
from agents.agent import ClientAgent, ServerAgent
import cmd
import json
from util.LGUtil import load_configuration_from_file, implements
from network.network import MessageListener, ServerQueue, LGNetworkServer, NetworkThread
from threading import Thread
from time import sleep
import os.path


# TODO Add dynamic pool increase and decrease

LG_VERSION = '0.0.4'
CONFIGURATION_FILE_NAME = "lg.config"

@implements(MessageListener)
class CommandLine(cmd.Cmd):
    """
    {
        "command": "login"
        "agent_name": "name"
        "ip" : "127.0.0.1"
        "port" : 35633
    }
    {
        "command": "logout"
        "agent_name": "name"
    }

    {
        "command": "start"
    }

    {
        "command": "started"
    }

    {
        "command": "stop"
    }

    {
        "command": "stopped"
    }

    {
        "command": "get_available_clients"
    }

    {
        "command": "client_info",
    }
    {
        "command": "client_info",
        "agent_name": "agent_name",
        "initialized": 0|1
        "client_name": "TestClient"
        "threadpoolsize": 4,
        "state": 0|1
    }
    {
        "command": "set_test_config",
        "simulate_client_number": 4,
        "client_name": "com.test.TestClient",
        "test_type": "INFINITE",
        "test_methods": [
            {
                "method_name": "name",
                "method_execution_number": 0,
                "method_params": [
                    "param1",
                    "param2"
                ]
            }
        ]
    }

    {
        "command": "error",
        "agent_name": "agent name",
        "error_message": "Error happened"
    }

    {
        "command": "message",
        "agent_name": "agent name",
        "message": "Sample message"
    }

    {
        "command": "get_statistics",
    }

    {
        "command": "statistics",
        "agent_name": "agent name",
        "statistics": {}
    }
    """

    def __init__(self):
        super().__init__(completekey='Tab', stdin=sys.stdin, stdout=sys.stdout)
        self._server_agent = None
        self._statistics_thread = None

    def do_load_file(self, line):
        if line is None or len(line) == 0:
            print('Missing file name. Type help load_file for details')
            return
        if not os.path.isfile(line):
            print('File {} does not exists'.format(line))
            return
        with open(line) as f:
            for _line in f:
                self.onecmd(_line)

    def help_load_file(self):
        print('Load commands from file. Usage:load_file <file_name>')

    def set_server_agent(self, server_agent: ServerAgent):
        self._server_agent = server_agent

    def do_start_statistics(self, line):
        if line is None or line == '':
            print('Missing parameter')
            return
        try:
            if self._statistics_thread is None:
                self._statistics_thread = Statistics(self._server_agent, sleep_seconds=3)
            self._statistics_thread.set_interval(int(line.strip()))
            self._statistics_thread.start()
        except Exception as e:
            print('Error. Time interval is wrong', str(e))

    def help_start_statistics(self):
        print('Starts statistics collecting from the agent:\n start_statistics <time interval in sec>')
        if self._statistics_thread.isAlive():
            return
        else:
            self._statistics_thread.start()

    def do_stop_statistics(self, line):
        if self._statistics_thread.isAlive():
            self._statistics_thread.terminate()

    def help_stop_statistics(self, line):
        print('Stop collecting statistics')

    def do_version(self, line):
        print("LG Version: %s\n Mixer 2014" % LG_VERSION)

    def do_start(self, agent_name):
        if agent_name is None or agent_name == '':
            print('Agent name is missing')
            return
        # check for client
        if not self._has_client(agent_name):
            print('Agent with name \'{name}\' is not connected'.format(name=agent_name))
            return
        _command = json.dumps({"command": "start"})
        self._server_agent.send(message=_command, to_agent=agent_name)

    def help_start(self):
        print("Starts the clients on and agent: \nstart <agent_name>")

    def help_stop(self):
        print("Stops the clients on an agent\n stop <agent_name>")

    def _has_client(self, agent_name: str) -> bool:
        _is_client = False
        for agents in self._server_agent.get_connected_clients():
            print(agents)
            if agent_name in agents:
                _is_client = True
                break
        return _is_client

    def do_stop(self, agent_name):
        if agent_name is None or agent_name == '':
            print('Agent name is missing')
            return
        if not self._has_client(agent_name):
            print('Agent with name \'{name}\' is not connected'.format(name=agent_name))
            return
        _command = json.dumps({"command": "stop"})
        self._server_agent.send(message=_command, to_agent=agent_name)

    def do_exit(self, line):
        if self._statistics_thread is not None and  self._statistics_thread.isAlive():
            self._statistics_thread.terminate()
        # send stop messages to all agents
        self._server_agent.send_stop_to_all_agents()
        self._server_agent.stop()
        sys.exit(0)

    def do_get_available_clients(self, agent_name=None):
        if agent_name is None or agent_name == '':
            print("Missing agent name")
            return
        _message = json.dumps({"command": "get_available_clients"})
        self._server_agent.send(message=_message, to_agent=agent_name)

    def help_get_available_clients(self):
        print("Gets the list of the available clients and methods on a given agent:\n"
              "get_available_clients <agent_name>")

    def do_set_test_config(self, param: str=None):
        if param is None or param == '':
            print("Missing parameters. Type 'help set_test_config'. ")
            return
        params = param.split(' ')

        if len(params) == 1 or params[0] is None or params[1] is None:
            print("Missing parameters. Type 'help set_test_config'. ")
            return
        # load the config file and send it to the client
        if not os.path.exists(params[1]):
            print('File does not exists:', params[1])
            return
        file = open(params[1], 'r')
        data = file.read()
        file.close()
        self._server_agent.send(message=data, to_agent=params[0])

    def do_set_group_test_config(self, param: str=None):
        if param is None or param == '':
            print("Missing parameters. Type 'help set_group_test_config'. ")
            return
        params = param.split(' ')

        if len(params) == 1 or params[0] is None or params[1] is None:
            print("Missing parameters. Type 'help set_test_config'. ")
            return
        # load the config file and send it to the client
        if not os.path.exists(params[1]):
            print('File does not exists:', params[1])
            return
        file = open(params[1], 'r')
        data = file.read()
        file.close()
        agent_names = self._server_agent.get_group_members(params[0])
        print(agent_names)
        for agent in agent_names:
            self._server_agent.send(message=data, to_agent=agent)

    def help_set_group_test_config(self, param: str=None):
        print("Sets the test configuration from file for a given group:\n"
              "set_group_test_config <group name> <config file name>")

    def help_set_test_config(self, param: str=None):
        print("Sets the test configuration from file for a given agent:\n"
              "set_test_config <agent_name> <config file name>")

    def do_get_connected_agents(self, line):
        # print(self._module.get_connected_clients())
        print('Connected agents')
        print('===================')
        _connected_clients = self._server_agent.get_connected_clients()
        for name in _connected_clients:
            print('{0:10} {1:3}'.format(name[0], name[1]))

    def help_get_connected_agents(self):
        print('Get the connected agent list')

    def do_create_group(self, line):
        self._server_agent.create_group(line)

    def help_create_group(self, line):
        print('Create agent group: create_group <group name>')

    def do_delete_group(self, line):
        self._server_agent.delete_group(line)

    def help_delete_group(self, line):
        print('Delete agent group: delete_group <group name>')

    def do_add_agents_to_group(self, line):
        # get the group
        group_name = line[0: line.index(' ')]
        agents = line[line.index(' ') + 1:]
        if len(agents.strip()) == 0:
            print('Type help for this command.')
            return
        self._server_agent.add_agents_to_group(group_name, agents)

    def help_add_agents_to_group(self):
        print('Add agents to group. add_agents_to_group <group name> <agents separated with comma>')

    def do_list_group_members(self, line):
        print(self._server_agent.list_group_members(line))

    def help_list_group_members(self):
        print('List group members')

    def do_list_groups(self, line):
        print(self._server_agent.get_groups())

    def help_list_groups(self, line):
        print('Lists available groups')

    def do_get_client_info(self, line):
        if line == '':
            print("Client name is empty")
            return

        _json_str = json.dumps({
            "command": "client_info"
        })
        self._server_agent.send(_json_str, to_agent=line)

    def help_get_client_info(self):
        print('Returns the methods and the parameters of the client')

    def emptyline(self):
        pass

    def arrived(self, message: str=None):
        """
        parse message and do the appropriate action
        :param message:
        :return:
        """

        _msg = json.loads(message)
        # print('Received message:', message)
        if _msg['command'] == 'login':
            self._server_agent.add_connected_agent((_msg['agent_name'], _msg['ip']))
            self._server_agent.register_agent(_msg['agent_name'], _msg['ip'], _msg['port'])

        elif _msg['command'] == 'logout':
            # log_info("Client connected...")
            for element in self._connected_clients:
                if element[0] == _msg['agent_name']:
                    self._server_agent.connected_clients.remove(element)
                    break

        elif _msg['command'] == 'available_clients':
            print('Available clients on agent ', _msg['agent_name'])
            print('================================================')
            for _client in _msg['available_clients']:
                print(_client)

        elif _msg['command'] == 'client_info':
            print('Agent information ', _msg['agent_name'])
            print('================================================')
            print('Initialized:', 'True' if _msg['initialized'] == 1 else 'False')
            print('Client name:', _msg['client_name'])
            print('Thread pool size:', _msg['threadpoolsize'])
            print('State', 'Running:' if _msg['state'] == 1 else 'Not running')

        elif _msg['command'] == 'statistics':
            with open('results.stat', 'a') as result_file:
                _d = _msg['statistics']
                s = ', '.join('{key}: {value}'.format(key=key, value=value) for key, value in _d.items())
                result_file.write(_msg['agent_name'] + "," + s + '\n')


class Statistics(Thread):

    def __init__(self, server_agent: ServerAgent, sleep_seconds: int=2):
        super().__init__()
        self._server_agent = server_agent
        self._terminate = False
        self._sleep_seconds = sleep_seconds

    def set_interval(self, interval: int):
        self._sleep_seconds = interval

    def run(self):
        # get the statistics from the agents
        _json_str = json.dumps({
            "command": "get_statistics",
        })
        while not self._terminate:
            self._server_agent.broadcast_message_to_agents(message=_json_str)
            sleep(self._sleep_seconds)

    def terminate(self):
        self._terminate = True


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-m', '--mode', dest='mode', choices=['server', 'client'])
    parser.add_argument('-f', '--file', dest='command_file')
    parser.add_argument('-n', '--name', dest='agent_name')
    parser.add_argument('-lp', '--local_port', dest='local_port')
    parser.add_argument('-ci', '--controller_ip', dest='controller_ip')
    parser.add_argument('-cp', '--controller_port', dest='controller_port')
    args = parser.parse_args()
    # initialize logger
    load_configuration_from_file(CONFIGURATION_FILE_NAME)
    # init server networking
    if args.mode == 'client':
        logging.info(
            "Running in client mode, server address %s, server port: %s" % (args.controller_ip, args.controller_port))
        client = ClientAgent(args.agent_name, args.controller_ip, int(args.controller_port))
        client.initialize()
        client.start()
    elif args.mode == 'server':
        logging.info("Running in server mode, ip:{} port:{}".format(socket.gethostbyname(socket.gethostname()), args.local_port))
        _cmd_line = CommandLine()
        _server_queue = ServerQueue()
        bind_address = socket.gethostname()
        if args.controller_ip:
            bind_address = args.controller_ip
        _lg_network_server = LGNetworkServer(_server_queue, (bind_address, int(args.local_port)), _cmd_line)
        _networkModule = NetworkThread(_lg_network_server)

        server = ServerAgent(_cmd_line,
                             _server_queue,
                             _lg_network_server,
                             _networkModule)
        _cmd_line.set_server_agent(server)
        server.initialize()
        server.start()
        _cmd_line.prompt = '(Lg) > '
        if args.command_file!='':
            pass
        _cmd_line.cmdloop()

