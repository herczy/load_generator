import json
import inspect
import logging
import abc
import collections

logger = None


def interface(clazz: type):
    new_clazz = abc.ABCMeta(clazz.__name__, clazz.__bases__, dict(clazz.__dict__))
    # implement the methods
    new_clazz.abstracts = {name: value for name, value in clazz.__dict__.items() if callable(value)}

    return new_clazz


def implements(clazz: abc.ABCMeta):
    def decorator(decorated_clazz: type):
        if not all(name in decorated_clazz.__dict__ for name in clazz.abstracts.keys()):
            raise NotImplementedError('Interface was not implemented correctly!') # "Witty remark"

        clazz.register(decorated_clazz)
        return decorated_clazz

    return decorator


def test_method(method):
    method.test_method = True
    return method


def get_client_configuration() -> str:
    with open("lg.config") as json_data:
        data = json.load(json_data)
        return data


def my_import(name: str):
    mod = __import__(name.rsplit('.', 1)[0])
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def get_clients_methods(client_array: collections.Sequence) -> dict:
    client_methods = collections.defaultdict(list)

    for client in client_array:
        clazz = my_import(client)
        c = clazz()
        methods = inspect.getmembers(c, predicate=inspect.ismethod)
        for method in methods:
            if getattr(method[1], 'test_method', False):
                # append a tuple, where the first element is
                # get the method parameters
                res = inspect.getfullargspec(method[1])
                res.args.insert(0, method[0])
                client_methods[client].append(tuple(res.args))

    return client_methods


def load_configuration_from_file(configuration_file='lg.config'):
    with open(configuration_file) as file:
        config_data = file.read()
        j_d = json.loads(config_data)
        set_log_level(j_d['log_level'])
        set_file_logging(j_d['log_file'])


def get_registered_clients_from_config(configuration_file='lg.config'):
    with open(configuration_file) as file:
        config_data = file.read()
        j_d = json.loads(config_data)
        return j_d['clients']


def set_log_level(log_level: int):
    if log_level == 0:
        logging.basicConfig(level=logging.ERROR)
    elif log_level == 1:
        logging.basicConfig(level=logging.WARNING)
    elif log_level == 2:
        logging.basicConfig(level=logging.INFO)
    elif log_level == 3:
        logging.basicConfig(level=logging.DEBUG)

    global logger
    logger = logging.getLogger('LG')


def set_file_logging(log_file_name: str):
    if len(log_file_name.strip()) > 0:
        global logger
        logging.basicConfig(level=logging.INFO, filename=log_file_name)
        logger = logging.getLogger('LG')


def log_info_str(message_string: str):
    logging.info(message_string)


def log_info(message_string: str):
    logging.info(message_string)


def log_warning(message_string: str):
    logger.warning(message_string)


def log_debug(message_string: str):
    logger.debug(message_string)


def log_error(message_string: str):
    logger.error(message_string)


class ClientQueue:
    def __init__(self):
        self._messages = []

    def get_next_message(self):
        if self._messages:
            return self._messages.pop(0)

    def messages_number(self) -> int:
        return len(self._messages)

    def clear_messages(self):
        del self._messages[:]

    def add_message(self, message: str=None):
        if message is None:
            return
        self._messages.append(message)


class ServerQueue:
    """
    Server Queue stores the messages which will must be sent to
    the connected clients
    """
    def __init__(self):
        self._queue = collections.defaultdict(list)

    def get_next_message(self, client_name: str):
        if self._queue[client_name]:
            return self._queue[client_name].pop(0)

    def messages_number(self, client_name) -> int:
        return len(self._queue[client_name])

    def clear_messages(self, client_name: str):
        del self._queue[client_name][:]

    def add_message(self, client_name, message: str):
        if client_name is None or message is None:
            return
        # print("Message to %s" % client_name)
        self._queue[client_name].append(message)


class Context:
    def __init__(self, client_queue: ClientQueue):
        self._client_queue = client_queue

    def send_message(self, message: str):
        self._client_queue.add_message(message)
