from threading import Thread
from util.LGUtil import log_info, log_error, Context, ClientQueue, my_import
import time


class ThreadPool:

    @property
    def is_running(self):
        return self._running

    @property
    def thread_number(self):
        return self._thread_number

    def __init__(self, thread_number=1, client_queue: ClientQueue=None):
        self._thread_number = thread_number
        self._threads = []
        self._running = False
        self._test_config = None
        self._test_client_name = None
        self._context = Context(client_queue)

    def initialize(self, with_test_client_name, with_test_config=None, with_thread_number=1):
        self._thread_number = with_thread_number
        self._test_config = with_test_config
        self._test_client_name = with_test_client_name

    def start_executors(self):
        if self._test_client_name is None:
            log_error("No client name was given.")
            return
        self._threads = [ExecutorThread(test_client=my_import(self._test_client_name)(), test_configuration=self._test_config, context=self._context)
                         for _ in range(self._thread_number)
                         ]

        if self._test_config is None:
            log_error("No configuration was given")
            return
        for i in range(self._thread_number):
            self._threads[i].start()
        self._running = True

    def stop_executors(self):
        for i in range(self._thread_number):
            self._threads[i].terminate()
        self._running = False


class ExecutorThread(Thread):

    def __init__(self, test_client=None, test_configuration: dict=None, context: Context=None):
        super().__init__()
        self._context = context
        self._test_client = test_client
        self._test_configuration = test_configuration
        self._test_client.initialize(test_configuration['parameters'], self._context)
        self._terminate = False

    def __del__(self):
        if self._test_client:
            self._test_client.uninitialize()

    def run(self):
        log_info('Threadpool has been started.')
        if self._test_configuration['test_type'] == 'INFINITE':
            self._run_infinite_test()

        elif self._test_configuration['test_type'] == 'SEQUENCE':
            self._run_test_once()

        elif self._test_configuration['test_type'] == 'TIME_LIMITED':
            self._run_time_limited_test()

    def _run_infinite_test(self):
        log_info("Running INFINITE configuration")
        while self._terminate is False:
            self._run_test_once()
        pass

    def _run_time_limited_test(self):
        log_info("Running TIME_LIMITED configuration")
        _time_to_run = self._test_configuration['time'] * 1000
        start_time = time.time()
        while (time.time() - start_time) < _time_to_run:
            self._run_test_once()
        log_info("Running TIME_LIMITED configuration, DONE")

    def _run_test_once(self):
        log_info("Running SEQUENCE configuration")
        test_methods = self._test_configuration['test_methods']
        for i in range(len(test_methods)):
            func = getattr(self._test_client, test_methods[i]['method_name'])
            for execution_number in range(test_methods[i]['method_execution_number']):
                try:
                    if len(test_methods[i]['method_params']) == 0:
                        func()
                    else:
                        log_info("Call method with params {}".format(str(test_methods[i]['method_params'])))
                        func(*test_methods[i]['method_params'])
                except Exception as exception:
                    log_error(str(exception))

    def terminate(self):
        self._terminate = True


# def do_run():
#     config = """
#     {
#         "command": "set_test_config",
#         "simulate_client_number": 4,
#         "client_name": "com.test.TestClient",
#         "test_type": "SEQUENCE",
#         "test_methods": [
#             {
#                 "method_name": "my_test_method_with_one_parameter",
#                 "method_execution_number": 1,
#                 "method_params": [
#                     "param1"
#                 ]
#             },
#             {
#                 "method_name": "my_test_method",
#                 "method_execution_number": 1,
#                 "method_params": []
#             },
#             {
#                 "method_name": "my_test_method_with_two_parameters",
#                 "method_execution_number": 1,
#                 "method_params": [
#                     "param1",
#                     "param2"
#                 ]
#             }
#         ]
#     }
#     """
#     thread_pool = ThreadPool()
#     thread_pool.initialize(ExampleClient(), with_test_config=json.loads(config), with_thread_number=3)
#     thread_pool.start_executors()
#
# if __name__ == '__main__':
#     do_run()
