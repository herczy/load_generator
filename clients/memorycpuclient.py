from util.LGUtil import test_method, implements, Context, log_info, interface
from clients.baseclient import BaseClient
import psutil
from threading import Thread, Lock
from collections import defaultdict

@interface
class DataListener:

    def data(self, cpu_memory: dict):
        """
        {'process_name':{'memory':23, 'cpu':23}
        }
        """
        pass


class WatchThread(Thread):
    def __init__(self, processes_to_watch, update_interval_second: int=3, data_listener: DataListener=None):
        super().__init__()
        self._update_second = update_interval_second
        self._finish_running = False
        self._processes_to_watch = processes_to_watch
        self._data_listener = data_listener

    def run(self):
        while not self._finish_running:
            _data = self._get_memory_and_cpu_data()
            if self._data_listener is not None:
                self._data_listener.data(cpu_memory=_data)

    def _get_memory_and_cpu_data(self):
        result = defaultdict(dict)
        for process in psutil.process_iter():
            if process.name() in self._processes_to_watch:
                result[process.name()] = {'memory': process.get_cpu_percent(interval=1), 'cpu':process.get_memory_info()}

        return result

    def terminate(self):
        self._finish_running = True


@implements(BaseClient)
@implements(DataListener)
class MemoryCPUMeasureClient:

    def __init__(self):
        self._processes_to_watch = []
        self._data_query_interval = 3
        self._latest_result = {}
        self._watch_thread = None
        self._lock = Lock()

    def initialize(self, parameters: dict, context: Context):
        log_info('Initialized with parameters:{}'.format(parameters))
        self._data_query_interval = parameters['intervals']
        self._processes_to_watch = parameters['processes']
        self._watch_thread = WatchThread(self._processes_to_watch, self._data_query_interval, data_listener=self)

    def uninitialize(self):
        self._watch_thread.terminate()

    @test_method
    def start_measurement(self):
        if not self._watch_thread.isAlive():
            self._watch_thread.start()
            print('Thread has been started..')

    def statistics(self) -> dict:
        with self._lock:
            return self._latest_result

    def data(self, cpu_memory: dict):
        with self._lock:
            self._latest_result = cpu_memory
