from util.LGUtil import test_method, implements, Context
from clients.baseclient import BaseClient
from random import randint


@implements(BaseClient)
class ExampleClient:
    def __init__(self):
        pass

    @test_method
    def my_test_method(self):
        print("my_test_method has been called")

    @test_method
    def my_test_method_with_one_parameter(self, str_param: str):
        print("my_test_method_with_one_parameter has been called")

    @test_method
    def my_test_method_with_two_parameters(self, str_param: str, str_param2: str):
        print("my_test_method_with_two_parameters has been called")

    def initialize(self, parameters: dict, context: Context):
        print('Initialized with parameters:', parameters)

    def uninitialize(self):
        pass

    def statistics(self) -> dict:
        return {'stat_param1': randint(0, 234),
                'stat_param2': randint(0, 3000),
                'stat_param3': randint(0, 300),
                'stat_param4': randint(0, 35),
                'stat_param5': randint(0, 799)
               }
