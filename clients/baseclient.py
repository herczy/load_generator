from util.LGUtil import interface, Context


@interface
class BaseClient:
    """
    This interface must be implemented by all
    test clients
    """
    def initialize(self, parameters: dict, context: Context):
        """
        This method will be executed after the object is
        created and the before the test is started
        """
        pass

    def uninitialize(self):
        """
        This method is execute when the test has finished
        """
        pass

    def statistics(self) -> dict:
        """
        This method is used to query statistics from the
        client
        Method must return a dictionary
        """
        pass
