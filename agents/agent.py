"""
This class is responsible for starting the
network layer, initialize the configuration and
keep the connection with the Controller module
"""

import json
import socket
from collections import defaultdict
from network.network import LGNetworkClient, ClientQueue, NetworkThread, MessageListener
from network.network import ServerQueue, LGNetworkServer
import util.LGUtil
from util.LGUtil import my_import, implements, log_info, set_log_level, set_file_logging, log_error
from pool.threadpool import ThreadPool


@implements(MessageListener)
class ClientAgent:
    def __init__(self, agent_name: str, controller_ip: str, controller_port: int):
        self._agent_name = agent_name
        self._controller_ip = controller_ip
        self._controller_port = controller_port
        self._network_thread = None
        self._lg_client = None
        self._client_queue = None
        self._network_module = None
        # configuration related variables
        self._registered_clients = []
        self._thread_pool = None
        # instance of the client which will perform the tests
        self._test_client = None
        self._test_client_name = None
        # if test configuration is set, then it is initialized
        self._initialized = 0  # 0: stopped, 1: started

    def send_message(self, message: str):
        self._client_queue.add_message(message)

    def initialize(self):
        # initialize the communication queue
        self._client_queue = ClientQueue()
        # initialize the Network layer
        self._lg_client = LGNetworkClient(self._client_queue, (self._controller_ip, self._controller_port), self)
        self._network_module = NetworkThread(self._lg_client)
        self._thread_pool = ThreadPool()
        # login into controller
        _json_str = json.dumps(
            {
                "command": "login",
                "agent_name": self._agent_name,
                "ip": socket.gethostbyname(socket.gethostname()),
                "port": self._lg_client.socket.getsockname()[1]
            }
        )
        log_info(_json_str)
        self._client_queue.add_message(_json_str)
        # get registered clients from config
        self._registered_clients = util.LGUtil.get_registered_clients_from_config()

    def start(self):
        self._network_module.start()
        _json_str = json.dumps(
            {
                "command": "started"
            }
        )
        self._client_queue.add_message(_json_str)

    def stop(self):
        self._thread_pool.stop_executors()
        _json_str = json.dumps(
            {
                "command": "stopped"
            }
        )
        self._client_queue.add_message(_json_str)

    def shutdown(self):
        pass

    def arrived(self, message: str=None):
        log_info("Client listener: Message arrived:" + message)
        # parse json
        msg = json.loads(message)
        if msg['command'] == 'get_available_clients':
            # log_info("Received command:%s" % msg['command'])
            response = self.get_available_client_names()
            log_info('available clients:' + response)
            self._client_queue.add_message(response)

        elif msg['command'] == 'start':
            if self._thread_pool is not None:
                self._thread_pool.start_executors()

        elif msg['command'] == 'stop':
            if self._thread_pool is not None:
                self._thread_pool.stop_executors()

        elif msg['command'] == 'set_test_config':
            self._set_test_configuration(msg)

        elif msg['command'] == 'get_statistics':
            if self._test_client is not None:
                _client_statistics = self._test_client.statistics()
                _json_str = json.dumps(
                    {
                        "command": "statistics",
                        "agent_name": self._agent_name,
                        "statistics": _client_statistics
                    }
                )
                log_info('sending statistics...')
                self._client_queue.add_message(_json_str)
            else:
                log_error("test client is None, cannot get statistics")

        elif msg['command'] == 'client_info':
            _json_str = json.dumps({
                "command": "client_info",
                "agent_name": self._agent_name,
                "initialized": self._initialized,
                "client_name": str(self._test_client_name),
                "threadpoolsize": self._thread_pool.thread_number,
                "state": self._thread_pool.is_running
            })
            self._client_queue.add_message(_json_str)

    def _set_test_configuration(self, configuration: dict=None):
        if configuration is None:
            log_error("Configuration is None")
            return
        # create the client
        self._test_client_name = configuration['client_name']
        self._test_client = my_import(configuration['client_name'])()
        # initialize with parameters
        # self._test_client.initialize(configuration['parameters'])
        self._thread_pool.initialize(self._test_client_name, configuration, configuration['simulate_client_number'])
        log_info('Client initialized, threadPool has been created!!')
        self._initialized = 1

    def get_available_client_names(self) -> str:
        # methods = util.LGUtil.get_registered_clients_from_config()
        return json.dumps(
            {"command": "available_clients",
             "agent_name": self._agent_name,
             "available_clients": self._registered_clients
            }
        )


class ServerAgent:
    def __init__(self,
                 message_listener: MessageListener,
                 server_queue: ServerQueue,
                 lg_server: LGNetworkServer,
                 network_module: NetworkThread
                ):
        # stores tuples (agent name, IP address)
        self._connected_clients = []
        self._message_listener = message_listener
        self._server_queue = server_queue
        self._networkModule = network_module
        self._lg_server = lg_server
        # store group name and the list of agent names
        self._groups = defaultdict(list)

    def initialize(self):
        pass

    def create_group(self, group_name: str):
        if group_name not in self._groups.keys():
            self._groups[group_name] = list()

    def delete_group(self, group_name: str):
        if group_name in self._groups:
            del self._groups[group_name]

    def get_groups(self):
        return ','.join([key for key in self._groups.keys()])

    def add_agents_to_group(self, group_name, agent_names):
        clients = agent_names.split(',')
        self._groups[group_name].extend(clients)

    def delete_agent_from_group(self, group_name: str, agent_name: str):
        if group_name not in self._groups:
            return
        self._groups[group_name].remove(agent_name)

    def get_group_members(self, group_name):
        return self._groups[group_name]

    def list_group_members(self, group_name):
        if group_name not in self._groups:
            return 'No group with name %s'.format(group_name)
        return str(self._groups[group_name])

    def register_agent(self, agent_name: str, ip_address: str, port_number: str):
        self._lg_server.register_agent(agent_name, ip_address, port_number)

    def get_next_message(self, agent_name: str):
        return self._server_queue.get_next_message(agent_name)

    def add_connected_agent(self, agent_data: tuple):
        self._connected_clients.append(agent_data)

    def get_connected_clients(self):
        return self._connected_clients

    def start(self):
        self._networkModule.start()

    def stop(self):
        if self._networkModule:
            self._networkModule.shutdown()
        self._lg_server.stop()

    def send(self, message: str, to_agent: str):
        self._server_queue.add_message(to_agent, message)

    def broadcast_message_to_agents(self, message: str):
        for _client in self._connected_clients:
            self.send(message=message, to_agent=_client[0])

    def send_stop_to_all_agents(self):
        _cmd = json.dumps({"command": "stop"})
        for _client in self._connected_clients:
            self.send(message=_cmd, to_agent=_client[0])



